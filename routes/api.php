<?php

use App\Models\Kelas;

use App\Http\Controllers\API\KelasController;
use App\Http\Controllers\API\EnrollsController;
use App\Http\Controllers\API\KontenDokumenController;
use App\Http\Controllers\API\KontenVideoController;
use App\Http\Controllers\API\KategoriController;
use App\Http\Controllers\API\KalenderController;
use App\Http\Controllers\API\UserVideoController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PassportAuthController;
use App\Http\Controllers\API\DownloadController;
use App\Http\Controllers\API\MataKuliahController;
use App\Http\Controllers\API\PertemuanController;
use App\Http\Controllers\API\ViewController;
use App\Http\Controllers\API\ArtikelController;
use App\Http\Controllers\API\IklanController;
use App\Http\Controllers\API\JobChannelController;
use App\Http\Controllers\API\ProfilController;
use App\Http\Controllers\API\AssignmentController;
use App\Http\Controllers\API\AssignmentFileController;
use App\Http\Controllers\API\AssignmentPilganController;
use App\Http\Controllers\API\AssignmentTextController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public routes
Route::post('/register', [PassportAuthController::class, 'register']);
Route::post('/login', [PassportAuthController::class, 'login']);
Route::get('/kelas', [KelasController::class, 'index']);
Route::get('/kelas/{id}', [KelasController::class, 'show']);
Route::get('/kelas/search/{name}', [KelasController::class, 'search']);
Route::get('/kelas/{id}/video', [KelasController::class, 'kelas_video']);
Route::get('/kelas/{id}/dokumen', [KelasController::class, 'kelas_dokumen']);
Route::post('/kelas/{id}/video', [KontenVideoController::class, 'store']);
Route::post('/kelas/{id}/dokumen', [KontenDokumenController::class, 'store']);
Route::get('/kelas/search/{name}', [KelasController::class, 'search']);

Route::get('/kategori', [KategoriController::class, 'index']);
Route::get('/kategori/{id}', [KategoriController::class, 'show']);
Route::get('/kategori/search/{name}', [KategoriController::class, 'search']);
Route::get('/kategori/{id}/video', [KategoriController::class, 'kelas_video']);
Route::get('/kategori/{id}/dokumen', [KategoriController::class, 'kelas_dokumen']);
// Route::post('/kategori/{id}/video', [KontenVideoController::class, 'store']);
// Route::post('/kategori/{id}/dokumen', [KontenDokumenController::class, 'store']);
// Route::get('/kategori/search/{name}', [KelasController::class, 'search']);

Route::get('/dokumen', [KontenDokumenController::class, 'index']);
Route::get('/dokumen/{id}', [KontenDokumenController::class, 'show']);
Route::get('/dokumen/search/{name}', [KontenDokumenController::class, 'search']);
Route::get('/kelas/{id}/dokumen/jumlah', [KontenDokumenController::class, 'jumlah_dokumen']);

Route::get('/video', [KontenVideoController::class, 'index']);
Route::get('/video/{id}', [KontenVideoController::class, 'show']);
Route::get('/video/search/{name}', [KontenVideoController::class, 'search']);
Route::get('/kelas/{id}/video/jumlah', [KontenVideoController::class, 'jumlah_video']);

//Route Artikel
Route::get('/artikel', [ArtikelController::class, 'index']);
Route::get('/artikel/show/{id}', [ArtikelController::class, 'show']);
Route::get('/artikel/search/{judul}', [ArtikelController::class, 'search']);
Route::post('/artikel/store', [ArtikelController::class, 'store']);
Route::put('artikel/update/{id}', [ArtikelController::class, 'update']);
Route::delete('artikel/delete/{id}', [ArtikelController::class, 'destroy']);
Route::get('/artikel/new', [ArtikelController::class, 'latest_article']);

//Route Iklan
Route::get('/iklan', [IklanController::class, 'index']);
Route::post('/iklan', [IklanController::class, 'store']);
Route::get('/iklan/{id}', [IklanController::class, 'show']);
Route::get('/iklan/{id}/download', [IklanController::class, 'download']);
Route::get('/iklan/{id}/view', [IklanController::class, 'view']);
Route::resource('iklan', IklanController::class);

//Route JobChannel
Route::get('/jobChannel', [JobChannelController::class, 'index']);
Route::post('/jobChannel', [JobChannelController::class, 'store']);
Route::get('/jobChannel/{id}', [JobChannelController::class, 'show']);
Route::get('/jobChannel/{id}/download', [JobChannelController::class, 'download']);
Route::get('/jobChannel/{id}/view', [JobChannelController::class, 'view']);

//Route Profil
Route::get('/profil', [ProfilController::class, 'index']);
Route::post('/profil', [ProfilController::class, 'store']);
Route::get('/profil/{id}', [ProfilController::class, 'show']);
Route::get('/profil/{id}/view', [PofilController::class, 'view']);


//Route Quiz
Route::get('/quiz', [QuizController::class, 'index']);
Route::get('/Pertemuan', [QuizController::class, 'Pertemuan']);
Route::get('/quiz/show/{id}', [QuizController::class, 'show']);

//Route Assignment File
Route::get('/assignment',[AssignmentController::class, 'index']);
Route::post('/assignment',[AssignmentController::class, 'store']);
Route::get('/assignment/{id}/download', [AssignmentController::class, 'download']);
Route::get('/assignment/{id}/view', [AssignmentController::class, 'view']);
Route::get('/assignment/{id}',[AssignmentController::class, 'show']);

//Route Assignment Pilgan
Route::get('assignmentPilgan', [AssignmentPilganController::class, 'index']);
Route::get('assignmentPilgan/{matkul}/{pertemuan}', [AssignmentPilganController::class, 'show']);

//Route Assignment Text
Route::get('/assignmentText',[AssignmentFileController::class, 'index']);

// Download and view Route
Route::get('download/{tipe}/{filename}', [DownloadController::class, 'index']);
Route::get('/dokumen/{id}/download', [KontenDokumenController::class, 'download']);
Route::get('/dokumen/{id}/view', [KontenDokumenController::class, 'view']);
Route::get('view/{filename}', [ViewController::class, 'index']);
Route::get('view2/{filename}', [ViewController::class, 'index2']);

Route::get('/mata-kuliah', [MataKuliahController::class, 'index']);
Route::get('/mata-kuliah/{id}', [MataKuliahController::class, 'findbyid']);

Route::get('/kalender', [KalenderController::class, 'index']);

// Protected routes
Route::group(['middleware' => ['auth:api']], function () {
    Route::put('/enroll/video/{id}', [UserVideoController::class, 'update']);
    Route::put('/enroll/dokumen/{id}', [UserDokumenController::class, 'update']);
    Route::get('/enroll', [EnrollsController::class, 'index']);
    Route::get('/enroll/{id}', [EnrollsController::class, 'findbyid']);
    Route::post('/enroll', [EnrollsController::class, 'store']);
    Route::get('/enroll/dokumen', [EnrollsController::class, 'enroll_dokumen']);
    Route::get('/enroll/video', [EnrollsController::class, 'enroll_video']);
    Route::delete('/unenroll/{id}', [EnrollsController::class, 'unenrolls']);
    Route::get('/pertemuan', [PertemuanController::class, 'index']);
    Route::get('/pertemuan/{id}', [PertemuanController::class, 'findbyid']);
    Route::get('/user-details', [PassportAuthController::class, 'userDetail']);
    Route::post('/kelas', [KelasController::class, 'store']);
    Route::put('/kelas/{id}', [KelasController::class, 'update']);
    Route::delete('/kelas/{id}', [KelasController::class, 'destroy']);
    Route::post('/logout', [AuthController::class, 'logout']);

    Route::put('/dokumen/{id}', [KontenDokumenController::class, 'update']);
    Route::delete('/dokumen/{id}', [KontenDokumenController::class, 'destroy']);

    Route::put('/video/{id}', [KontenVideoController::class, 'update']);
    Route::delete('/video/{id}', [KontenVideoController::class, 'destroy']);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
