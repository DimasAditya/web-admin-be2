<?php
use App\Http\Controllers\KontenVideoController;
use App\Http\Controllers\KontenDokumenController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\MataKuliahController;
use App\Http\Controllers\IklanController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\JobChannelController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AssignmentController;
use App\Http\Controllers\UserAssignmentController;
// use App\Http\Controllers\AssignmentFileController;
// use App\Http\Controllers\AssignmentPilganController;
// use App\Http\Controllers\AssignmentTextController;
use App\Http\Controllers\AksesKelasMahasiswaController;
use App\Http\Controllers\AksesKelasDosenController;
use App\Http\Controllers\API\QuizController;
use App\Http\Controllers\DashboardController;
use App\Models\AssignmentPilgan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertemuanController;
use App\Http\Controllers\QuizController as ControllersQuizController;
use App\Models\Assignment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/fe', function () {
    return view('dosen.fe.index');
})->name('dashboard');
Route::get('/fe-file ', function () {
    return view('dosen.fe.file');
})->name('dashboard');
Route::get('/fe-pilgan ', function () {
    return view('dosen.fe.pilgan');
})->name('dashboard');
// Route::get('/lihat-pertemuan', function () {
//     return view('admin.pertemuan.index');
// })->name('dashboard');
Route::get('/fe-text', function () {
    return view('dosen.fe.text');
})->name('dashboard');
Route::get('/lihat-assignment', function () {
    return view('admin.assignment.lihat');
})->name('dashboard');


Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

// Route::get('/assignemntText/{id}/create', [AssignmentTextController::class, 'tambahAssignmentText'])->name('tambahAssignmentText');
// Route::post('/assignmentText/store', [AssignmentTextController::class, 'storeAssignmentText'])->name('storeAssignmentText');
// Route::get('/assignmentText/edit/{id}', [AssignmentTextController::class, 'edit'])->name('editAssignmentText');
// Route::post('/assignmentText/update/{id}', [AssignmentTextController::class, 'update'])->name('updateAssignmentText');
// Route::get('/assignmentText/destroy/{id}', [AssignmentTextController::class, 'destroy'])->name('destroyAssignmentText');

// // Route::get('/assignemntFile/{id}/create', [AssignmentFileController::class, 'tambahAssignmentFile'])->name('tambahAssignmentFile');
// // Route::post('/assignmentFile/store', [AssignmentFileController::class, 'storeAssignmentFile'])->name('storeAssignmentFile');
// Route::get('/assignmentFile/edit/{id}', [AssignmentFileController::class, 'edit'])->name('editAssignmentFile');
// Route::post('/assignmentFile/update/{id}', [AssignmentFileController::class, 'update'])->name('updateAssignmentFile');
// Route::get('/assignmentFile/destroy/{id}', [AssignmentFileController::class, 'destroy'])->name('destroyAssignmentFile');

// Route::get('/assignment', [AssignmentPilganController::class, 'index'])->name('assignmentPilgan');
// Route::get('/assignemntPilgan/{id}/create', [AssignmentPilganController::class, 'tambahAssignmentPilgan'])->name('tambahAssignmentPilgan');
// Route::post('/assignmentPilgan/store', [AssignmentPilganController::class, 'storeAssignmentPilgan'])->name('storeAssignmentPilgan');
// Route::get('/assignmentPilgan/edit/{id}', [AssignmentPilganController::class, 'edit'])->name('editAssignmentPilgan');
// Route::post('/assignmentPilgan/update/{id}', [AssignmentPilganController::class, 'update'])->name('updateAssignmentPilgan');
// Route::get('/assignmentPilgan/destroy/{id}', [AssignmentPilganController::class, 'destroy'])->name('destroyAssignmentPilgan');
// Route::get('/assignmentPilgan/{id}/show', [AssignmentPilganController::class, 'show'])->name('showPilgan');
// Route::post('/assignmentPilgan/import', [AssignmentPilganController::class, 'soalPilgan'])->name('soalPilgan');
// Route::get('/Assignment/{id}/assignmentPilgan/data', [AssignmentPilganController::class, 'data'])->name('data');

//Route Assignment File
Route::get('/assignmentFile/{id}/create', [AssignmentController::class, 'viewstoreFile'])->name('viewAssignmentFile');
Route::post('/assignment/store', [AssignmentController::class, 'store'])->name('AssignmentStore');
Route::get('/assignment/destroy/{id}', [AssignmentController::class, 'destroy'])->name('AssignmentDestroy');

//Route Assignment Text
Route::get('/assignmentText/{id}/create', [AssignmentController::class, 'tambahAssignmentText'])->name('tambahAssignmentText');
Route::post('/assignmentText/store', [AssignmentController::class, 'storeAssignmentText'])->name('storeAssignmentText');

//Route Assignment Pilihan Ganda
Route::get('/assignmentPilgan/{id}/create', [AssignmentController::class, 'tambahAssignmentPilgan'])->name('tambahAssignmentPilgan');
Route::post('/assignmentPilgan/store', [AssignmentController::class, 'storeAssignmentPilgan'])->name('storeAssignmentPilgan');
Route::get('/assignmentPilgan/{id}/show', [AssignmentController::class, 'show'])->name('showPilgan');
Route::post('/assignmentPilgan/import', [AssignmentController::class, 'importAssignmentPilgan'])->name('importAssignmentPilgan');
Route::get('/assignmentPilgan/{id}/show', [AssignmentController::class, 'showPilgan'])->name('showPilgan');

Route::post('/pertemuan/store', [PertemuanController::class, 'store'])->name('storePertemuan');
Route::get('/pertemuan/destroy/{id}', [PertemuanController::class, 'destroy'])->name('hapusPertemuan');
Route::get('/pertemuan/lihat-pertemuan/{id}', [PertemuanController::class, 'detail'])->name('detailPertemuan');
Route::get('/tambahKuis/{id}/create', [AssignmentController::class, 'tambahKuis'])->name('tambahKuis');
Route::post('/pertemuan/import', [PertemuanPilganController::class, 'soalQuiz'])->name('soalQuiz');
Route::get('/showAssignment/{id}', [UserAssignmentController::class, 'showAssignment'])->name('showAssignment');

//Quiz
Route::post('/QuizImport', [ControllersQuizController::class,'QuizImport'])->name('QuizImport');
Route::get('/quiz/destroy/{id}', [ControllersQuizController::class, 'destroy'])->name('QuizDestroy');

// Route::get('showKelas/{id)', [KelasController::class, 'show'])->name('showKelas');

//Route Login
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('dashboard');
    Route::get('/form', function () {
        return view('form');
    })->name('form');
    Route::get('/tab', function () {
        return view('tab');
    })->name('form');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
 
});

Route::resource('kontenVideo', KontenVideoController::class);
Route::resource('kontenDokumen', KontenDokumenController::class);
Route::resource('kelas', KelasController::class);
Route::resource('mataKuliah', MataKuliahController::class);
Route::resource('artikel', ArtikelController::class);
Route::resource('iklan', IklanController::class);
Route::resource('profil', ProfilController::class);
Route::resource('jobChannel', JobChannelController::class);
// Route::resource('assignment', AssignmentController::class);
Route::resource('assignmentFile', AssignmentFileController::class);
//Route::resource('assignmentPilgan', AssignmentPilganController::class);
Route::resource('assignmentText', AssignmentTextController::class);

Route::middleware(['auth', 'role:dosen'])->group(function () {
    Route::get('/dosen', [DashboardController::class, 'dosenDashboard'])
            ->name('dosen.dashboard');
    Route::get('/matkul', function () {
        return view('dosen.assignment.index');
    });
    Route::get('/showMatkul', function () {
        return view('dosen.tugas.tambahTugas');
    });
    Route::get('/showUjian', function () {
        return view('dosen.tugas.tambahUjian');
    });
    Route::get('/show', function () {
        return view('dosen.tugas.show');
    });

    Route::get('/assignment/{id}', [DashboardController::class, 'assignment'])->name('assignment');


    Route::get('/assignment/{id}/show', [DashboardController::class, 'show']);
    Route::get('/assignment/{id}/pilganDetail', [DashboardController::class, 'pilganDetail'])->name('pilganDetail');
    Route::get('/assignment/{id}/textDetail', [DashboardController::class, 'textDetail'])->name('textDetail');
    Route::get('/assignment/{id}/fileDetail', [DashboardController::class, 'fileDetail'])->name('fileDetail');
    Route::get('/showPilgan/{id}', [AssignmentPilganController::class, 'show'])->name('showPilgan');
});

//Role Admin
Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::get('/admin', function () {
        return view('admin.index');
    })->name('admin.dashboard');
    Route::get('/dataDosen', [DashboardController::class, 'dataDosen'])->name('dataDosen');
    Route::get('/dataMahasiswa', [DashboardController::class, 'dataMahasiswa'])->name('dataMahasiswa');
    Route::resource('akseskelasMahasiswa', AksesKelasMahasiswaController::class);
    Route::resource('akseskelasDosen', AksesKelasDosenController::class);
    Route::resource('kategori', KategoriController::class);
});

//Role User
Route::middleware(['auth', 'role:mahasiswa'])->group(function () {
    Route::get('/user', function () {
        return view('user');
    })->name('user.dashboard');
});

// Route::get('/linkstorage', function () {
//     Artisan::call('storage:link');
//     return '<h1>Storage Linked</h1>';
// });



