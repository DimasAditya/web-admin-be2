<?php

namespace App\Providers;
use App\Models\Kelas;
use App\Models\AksesKelas;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;

use phpDocumentor\Reflection\Types\Resource_;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        // Blade::if('featured', function($post){
        //     return $post->featured();
        // });
        // View::share('AksesKelas', AksesKelas::all());
    }
}
