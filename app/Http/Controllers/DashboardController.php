<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Kelas;
use App\Models\AksesKelas;
use App\Models\Assignment;
use App\Models\AssignmentText;
use App\Models\AssignmentPilgan;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dosenDashboard()
    {
        $Assignment = Assignment::where('user_id',Auth::user()->id)->get();
// dd($Assignment, Auth::user()->id);
        $user = User::has('akseskelas')->get();
        return view('dosen.index', compact('user','Assignment'));
    }

    public function dataDosen()
    {
        $user = User::where('role','dosen')->get();
        return view('admin.dataKelas.dosen.index', compact('user'));
    }

    public function dataMahasiswa()
    {
        $user = User::where('role','mahasiswa')->get();
        return view('admin.dataKelas.mahasiswa.index', compact('user'));
    }

    public function assignment($id)
    {
        $kelas = AksesKelas::find($id);
        $AssignmentFile = AssignmentFile::where('kelas_id',$id)->get();
        $AssignmentText = AssignmentText::where('kelas_id',$id)->get();
        $AssignmentPilgan = AssignmentPilgan::where('kelas_id',$id)->get();
        // dd($kelas->kelas_id);
        // dd($kelas->kelas_id,$kelas,$AssignmentFile->judul);
        return view('dosen.assignment.index', compact('kelas','AssignmentFile','AssignmentText','AssignmentPilgan'));
    }

    public function pilganDetail()
    {
        $assignmentPilgan = AssignmentPilgan::all();
        return view('dosen.assignment.pilgan.index', compact('assignmentPilgan'));
    }

    public function fileDetail()
    {
        $assignmentFile = AssignmentFile::all();
        return view('dosen.assignment.file.index', compact('assignmentFile'));
    }

    public function textDetail()
    {
        $assignmentText = AssignmentText::all();
        return view('dosen.assignment.text.index', compact('assignmentText'));
    }
}
