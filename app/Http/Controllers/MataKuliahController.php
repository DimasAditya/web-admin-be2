<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Kategori;
use App\Models\Kelas;
use App\Models\KontenDokumen;
use App\Models\KontenVideo;
use App\Models\MataKuliah;
use App\Models\Pertemuan;
use App\Models\Quiz;
use Illuminate\Http\Request;

class MataKuliahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $mataKuliah = MataKuliah::all();
        $kategori = kategori::all();
        return view('admin.matkul.show', compact('mataKuliah', 'kategori'));
    }

    public function create()
    {
        $mataKuliah = MataKuliah::all();
        $kategori = kategori::all();
        $kelas = Kelas::all();
        return view('admin.matkul.tambah',compact('mataKuliah', 'kategori','kelas'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'sks' => 'required',
            'kategori_id' => 'required',
            'kelas_id' => 'required',
        ]);
        MataKuliah::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'sks' => $request->sks,
            'kategori_id' => $request->kategori_id,
            'kelas_id' => $request->kelas_id,
        ]);
        //notify()->success('Kelas berhasil ditambahkan!');
        return back()
            ->with('success', 'Mata kuliah Berhasil Ditambahkan');
    }

    public function show($id)
    {
        $mataKuliah = MataKuliah::where('id', $id)->first();
        $kategori = Kategori::all();
        $kontenDokumen = kontenDokumen::where('matkul_id', $id)->get();
        $kontenVideo = kontenVideo::where('matkul_id', $id)->get();
        $mataKuliahselect = MataKuliah::all();
        $assignment = Assignment::where('matkul_id', $id)->get();
        $quiz = Quiz::where('matkul_id', $id)->get();
        $pertemuan = Pertemuan::all();
        $pertemuanselect = Pertemuan::where('matkul_id', $id)->get();
        return view('admin.matkul.show', compact('kategori','mataKuliahselect','kontenDokumen','kontenVideo', 'assignment','pertemuan','mataKuliah', 'quiz', 'pertemuanselect'));
    }


    public function edit($id)
    {
        $kategori = kategori::all();
        $matakuliah = MataKuliah::find($id);
        return view('admin.matkul.edit', compact('matakuliah','kategori'));
    }

    public function update(Request $request, $id)
    {
        $mata_kuliah = MataKuliah::findOrFail($id);
        $mata_kuliah->kategori_id = $request->kategori_id;
        $mata_kuliah->judul = $request->judul;
        $mata_kuliah->deskripsi = $request->deskripsi;
        $mata_kuliah->sks = $request->sks;
        $mata_kuliah->kelas_id = $request->kelas_id;
        $mata_kuliah->save();
        //notify()->success('Kelas berhasil diedit!');
        return redirect()->route('kelas.show',$request->kelas_id)
        ->with('edit', 'Matkul Berhasil Diedit');
    }

    public function destroy($id)
    {
        MataKuliah::where('id', $id)->delete();
        //notify()->success('Kelas berhasil dihapus!');
        return redirect()->back()
            ->with('delete', 'Matkul Berhasil Dihapus');
    }
}
