<?php

namespace App\Http\Controllers;
use App\Models\KontenVideo;
use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\MataKuliah;
use App\Models\Kategori;
class KontenVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
        $kontenVideo = KontenVideo::all();
        $judul = MataKuliah::get();
        return view('admin.kontenVideo.index',['judul'=>$judul], compact('kontenVideo','judul'));
    }

    public function create()
    {
        $kelas = MataKuliah::all();
        $kategori = Kategori::all();
        return view('admin.kontenVideo.tambah',compact('kelas','kategori'));
    }

    /**
     * Get Youtube video ID from URL
     *
     * @param string $url
     * @return mixed Youtube video ID or FALSE if not found
     */
    public function getYoutubeIdFromUrl($url)
    {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $qs);
            if (isset($qs['v'])) {
                return $qs['v'];
            } else if (isset($qs['vi'])) {
                return $qs['vi'];
            }
        }
        if (isset($parts['path'])) {
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path) - 1];
        }
        return false;
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'link' => 'required',
            'matkul_id' => 'required',
            'kategori_id' => 'required',
        ]);
        KontenVideo::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'link' => $this->getYoutubeIdFromUrl($request->link),
            'matkul_id' => $request->matkul_id,
            'kategori_id' => $request->kategori_id,
        ]);
        //notify()->success('Konten Video berhasil ditambahkan!');
        return redirect()->route('mataKuliah.show', $request->matkul_id)
            ->with('success', 'Konten Video Berhasil Ditambahkan');
    }

    public function show($id)
    {
        $kontenVideos = KontenVideo::where('id', $id)->first();
        return view('admin.kontenVideo.show', compact('kontenVideo'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function edit($id)
    {
        $kontenVideo = KontenVideo::find($id);
        $kelas = MataKuliah::all();
        $kategori = Kategori::all();
        return view('admin.kontenVideo.edit', compact('kontenVideo','kelas','kategori'));
    }

    public function update(Request $request, $id)
    {
        $kontenVideo = KontenVideo::findOrFail($id);
        $kontenVideo->judul = $request->judul;
        $kontenVideo->deskripsi = $request->deskripsi;
        $kontenVideo->link = $this->getYoutubeIdFromUrl($request->link);
        $kontenVideo->matkul_id = $request->matkul_id;
        $kontenVideo->kategori = $request->kategori;
        $kontenVideo->save();
        //notify()->success('Konten Video berhasil diedit!');
        return redirect()->route('kontenVideo.index')
        ->with('edit', 'Konten Video Berhasil Diedit');
    }

    public function destroy($id)
    {
        KontenVideo::where('id', $id)->delete();
        //notify()->success('Konten Video berhasil dihapus!');
        return redirect()->route('kontenVideo.index')
            ->with('delete', 'Konten Video Berhasil Dihapus');
    }
}