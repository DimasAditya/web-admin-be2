<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use Illuminate\Support\Facades\Storage;
use App\Imports\QuizImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function QuizImport(Request $request)
    {

        //$assignmentPilgan = AssignmentPilgan::all();
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);

        //$assignmentPilgan = new AssignmentPilgan();
        $file = $request->file('file');
        $judul = $request->judul;
        $deskripsi = $request->deskripsi;
        $deadline = $request->deadline;
        $pertemuan_id = $request->pertemuan_id;
        $matkul_id = $request->matkul_id;
        $file_name = rand() . $file->getClientOriginalName();
        $file->storeAs('public/Quiz', $file_name);
        // dd($pertemuan_id, $matkul_id, $file);
        Excel::import(new QuizImport($judul, $deskripsi, $deadline, $pertemuan_id, $matkul_id), public_path('/storage/Quiz/' . $file_name));
        Storage::delete("public/Quiz/$file_name");
        // dd($soal_id);

        return redirect()->back()
            ->with('success', 'Quiz Berhasil diimport');
    }

    public function destroy($id)
    {
        Quiz::where('id', $id)->delete();
        //notify()->success('Kelas berhasil dihapus!');
        return redirect()->back()
            ->with('delete', 'Quiz Berhasil Dihapus');
    }
}
