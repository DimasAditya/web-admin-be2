<?php

namespace App\Http\Controllers;
use App\Models\Assignment;
use App\Models\UserAssignment;

use Illuminate\Http\Request;

class UserAssignmentController extends Controller
{
    public function showAssignment($id)
    {
        $assignment = Assignment::find($id);
        $userAssignment = UserAssignment::all();
        // dd($assignment);
        return view('admin.assignment.lihat', compact('assignment', 'userAssignment'));
    }
}
