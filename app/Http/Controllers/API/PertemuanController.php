<?php

namespace App\Http\Controllers\API;

use App\Models\Pertemuan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PertemuanResource;
use App\Http\Resources\PertemuanCollection;

class PertemuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->matkul){
            $meet = Pertemuan::where('matkul_id', $request->matkul)->get();
        }else{
            $meet =  Pertemuan::all();
        }
        return new PertemuanCollection($meet);
    }

    public function findbyid($id)
    {
        $meet = Pertemuan::find($id);
        return new PertemuanResource($meet);
    }

}
