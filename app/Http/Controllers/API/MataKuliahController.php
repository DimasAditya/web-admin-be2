<?php

namespace App\Http\Controllers\API;

use App\Models\MataKuliah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MataKuliahResource;
use App\Http\Resources\MataKuliahCollection;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->kelas){
            $matkul = MataKuliah::where('kelas_id', $request->kelas)->get();
        }else{
            $matkul =  MataKuliah::all();
        }
        return new MataKuliahCollection($matkul);
    }

    public function findbyid($id)
    {
        $meet = MataKuliah::find($id);
        return new MataKuliahResource($meet);
    }
}
