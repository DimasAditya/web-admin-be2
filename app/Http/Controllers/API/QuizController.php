<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quiz;
use App\Models\Pertemuan;
class QuizController extends Controller
{
    public function index()
    {
        //
        $quiz = Quiz::all();
        // dd($quiz);
        return response()->json([
            "message" => "Success",
            "data" => $quiz
        ], 200);
    }

    public function Pertemuan()
    {
        //
        $Pertemuan = Pertemuan::all();
        // dd($quiz);
        return response()->json([
            "message" => "Success",
            "data" => $Pertemuan
        ], 200);
    }

    public function show($id)
    {
        $quiz = Quiz::find($id);
        if (!$quiz) {
            return response()->json([
                'message' => '404 Not Found'
            ], 400);
        }
        return response()->json($quiz, 200);
    }
}
