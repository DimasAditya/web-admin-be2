<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Kelas;
use App\Models\KontenVideo;
use App\Models\KontenDokumen;
use App\Models\UserVideo;
use App\Models\UserDokumen;

class EnrollsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $kelas = Kelas::find($this->kelas_id);
        $listvideo = $this->get_video;
        $listdokumen = $this->get_dokumen;
        $countvideo = $listvideo->count();
        $countdokumen = $listdokumen->count();
        $uvideo = $listvideo->where("iscomplete", true)->count();
        $udokumen = $listdokumen->where("iscomplete", true)->count();
        return [
            'id' => $this->id,
            //'kelas_id' => $this->kelas_id,
            'count' =>  $countvideo + $countdokumen,
            'count_complete' =>  $uvideo + $udokumen,
            'iscomplete' => $this->iscomplete,
            'nama_kelas'=> $kelas->nama,
            'kategori_kelas'=> $kelas->kategori->nama_kategori,
            //'video' => $this->get_video,
        ];
    }

    public function with($request)
    {
        return [
            "error" => false,
            "message" => "success",
            //'kelas' => $kelas,
        ];
    }
}
