<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KalenderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $tanggal =  explode('-', $this->deadline);
        return [
            'namaEvent' => $this->judul,
            'jenisEvent' => 'assignment',
            // 'assignment' => $this->assignment,
            'waktuSelesai' => date('d-m-Y', strtotime($this->deadline)),
            // 'tahun' => $tanggal[0],
            // 'bulan' => $tanggal[1],
            // 'tanggal' => $tanggal[2],
            'pertemuan_id' => $this->pertemuan_id,
            'matkul_id' => $this->matkul_id,
        ];
    }

    public function with($request)
    {
        return [
            "error" => false,
            "message" => "success",
            //'kelas' => $kelas,
        ];
    }

}
