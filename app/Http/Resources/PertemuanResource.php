<?php

namespace App\Http\Resources;
use App\Models\UserVideo;
use App\Models\UserDokumen;

use Illuminate\Http\Resources\Json\JsonResource;

class PertemuanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arr = $this->kontenVideo_id;
        $arr2 = $this->kontenDokumen_id;
        foreach ($arr as $item) { //foreach element in $arr
            $uses = $item['id'];
            $fin = UserVideo::where('konten_video_id', $uses);
            
        }
        return [
            'id' => $this->id,
            'urutan' => $this->pertemuan,
            'deskripsi' => $this->deskripsi,
            'matkul_id' => $this->matkul_id,
            'konten_video' => $fin,
            'konten_dokumen' => $this->kontenDokumen_id,
            //'video' => $this->get_video,
        ];
    }

    //public static $wrap = 'pertemuan';

    public function with($request)
    {
        return [
            "error" => false,
            "message" => "success",
        ];
    }
}
