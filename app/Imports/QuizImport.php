<?php

namespace App\Imports;

use App\Models\AssignmentPilganSoal;
use App\Models\AssignmentPilgan;
use App\Models\MataKuliah;
use App\Models\Quiz;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class QuizImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    protected $judul;
    protected $deskkripsi;
    protected $deadline;
    protected $matkul_id;
    protected $pertemuan_id;

    public function __construct(string $judul, $deskripsi, $deadline, $matkul_id, $pertemuan_id)
    {
    $this->judul = $judul;
    $this->deskripsi = $deskripsi;
    $this->deadline = $deadline;
    $this->pertemuan_id = $pertemuan_id;
    $this->matkul_id = $matkul_id;
    $this->pertemuan_id = $pertemuan_id;
    }

    public function model(array $row)
    {
        return new Quiz([
            'no' => $row[0],
            'soal' => $row[1],
            'opsi_a' => $row[2],
            'opsi_b' => $row[3],
            'opsi_c' => $row[4],
            'opsi_d' => $row[5],
            'opsi_e' => $row[6],
            'jawaban' => $row[7],
            'pertemuan_id' => $this->pertemuan_id,
            'matkul_id' => $this->matkul_id,
            'judul' => $this->judul,
            'deskripsi' => $this->deskripsi,
            'deadline' => $this->deadline
        ]);
    }
}
