<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVideo extends Model
{
    use HasFactory;
    protected $table = 'uservideo';
    protected $fillable = [
        'progress',
        'iscomplete',
        'user_id',
        'enrolls_id',
        'konten_video_id',
    ];

    protected $primaryKey = 'id';

    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function get_video()
    {
        return $this->hasOne(KontenVideo::class);
    }

    public function kelas()
    {
        return $this->belongsTo(kelas::class, 'kelas_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }

    public function video()
    {
        return $this->belongsTo(KontenVideo::class, 'konten_video_id', 'id');
    }
}
