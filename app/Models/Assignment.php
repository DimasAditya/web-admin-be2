<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use app\Models\Pertemuan;

class Assignment extends Model
{
    use HasFactory;

    protected $table = 'assignment';
    protected $fillable = [
        'judul',
        'pertemuan_id',
        'deskripsi',
        'text',
        'file',
        'deadline',
        'user_id',
        'matkul_id'
    ];

    protected $primaryKey = 'id';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function matkul()
    {
        return $this->belongsTo(MataKuliah::class, 'matkul_id', 'id');
    }

    public function pertemuan()
    {
        return $this->belongsTo(Pertemuan::class, 'pertemuan_id', 'id');
    }
}
