<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\KontenDokumen;
use App\Models\KontenVideo;

class Enrolls extends Model
{
    use HasFactory;
    protected $table = 'enrolls';
    protected $fillable = [
        'user_id',
        'kelas_id',
        'iscomplete',
        // 'kategori,'
    ];

    protected $primaryKey = 'id';

    protected $casts = [
        'iscomplete' => 'boolean',
    ];

    public function get_dokumen()
    {
    	return $this->hasMany(userDokumen::class);
    }

    public function get_video()
    {
    	return $this->hasMany(userVideo::class);
    }

    public function get_kelas()
    {
    	return $this->belongsTo(Kelas::class);
    }

    public function kelas()
    {
        return $this->belongsTo(kelas::class, 'kelas_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(user::class, 'user_id', 'id');
    }
}
