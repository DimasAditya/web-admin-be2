<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAssignment extends Model
{
    use HasFactory;
    protected $table = 'user_assignment';
    protected $fillable = [
        'assignment',
        'grade',
        'user_id',
        'pertemuan_id',
        'matkul_id',
        'iscomplete',
    ];

    public function matakuliah()
    {
        return $this->belongsTo(MataKuliah::class, 'matkul_id', 'id');
    }

    public function pertemuan()
    {
        return $this->belongsTo(Pertemuan::class, 'pertemuan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // public function get_user()
    // {
    //     return $this->hasMany(User::class);
    // }
}
