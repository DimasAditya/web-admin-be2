<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KontenVideo extends Model
{
    use HasFactory;
    protected $table = 'konten_video';
    protected $fillable = [
        'judul',
        'deskripsi',
        'link',
        'matkul_id',
        'kategori_id',
    ];

    protected $primaryKey = 'id';


    public function matkul()
    {
        return $this->belongsTo(MataKuliah::class, 'matkul_id', 'id');
    }
    
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'katgeori_id', 'id');
    }
}

