<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;
    protected $table = 'quiz';
    protected $fillable = [
        'judul',
        'deskripsi',
        'deadline',
        'no',
        'soal',
        'opsi_a',
        'opsi_b',
        'opsi_c',
        'opsi_d',
        'opsi_e',
        'jawaban',
        'pertemuan_id',
        'matkul_id'
    ];

    
    protected $primaryKey = 'id';

    public function pertemuan()
    {
        return $this->belongsTo(Pertemuan::class, 'pertemuan_id', 'id');
    }

    public function matkul()
    {
        return $this->belongsTo(MataKuliah::class, 'matkul_id', 'id');
    }
}
