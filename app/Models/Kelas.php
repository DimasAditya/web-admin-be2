<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\KontenDokumen;
use App\Models\KontenVideo;
use App\Models\Kategori;
use App\Models\AssignmentFile;
use App\Models\AssignmentText;
use App\Models\AssiigmentPilgan;

class Kelas extends Model
{
    use HasFactory;
    protected $table = 'kelas';
    protected $fillable = [
        'nama',
        'deskripsi',
    ];  

    protected $primaryKey = 'id';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function get_content()
    {
    	return $this->hasMany(Content::class);
    }

    public function get_dokumen()
    {
    	return $this->hasMany(KontenDokumen::class);
    }

    public function get_video()
    {
    	return $this->hasMany(KontenVideo::class);
    }

    public function get_file()
    {
        return $this->hasMany(AssignmentFile::class);
    }

    public function get_pilgan()
    {
        return $this->hasMany(AssignmentPilgan::class);
    }

    public function get_text()
    {
        return $this->hasMany(AssignmentText::class);
    }

    public function AksesKelas()
    {
        return $this->belongsToMany(User::class,'akses_kelas','user_id','kelas_id');
    }
}
