<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kelas;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::create([
            'id' => '1',
            'nama' => 'Credit',
            'deskripsi' => 'Ini kelas kredit',
            //'kategori_id' => '1'
        ]);    
        
        Kelas::create([
            'id' => '2',
            'nama' => 'Sales',
            'deskripsi' => 'Ini kelas sales',
            //'kategori_id' => '1'
        ]);   

        Kelas::create([
            'id' => '3',
            'nama' => 'Collection',
            'deskripsi' => 'Ini kelas collection',
            //'kategori_id' => '1'
        ]);  
    }
}

