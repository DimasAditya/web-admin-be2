<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertemuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertemuan', function (Blueprint $table) {
            $table->id();
            $table->integer('pertemuan');
            $table->string('judul');
            $table->string('deskripsi');
            $table->foreignId("matkul_id")->constrained("mata_kuliah")->onDelete("cascade")->onUpdate("cascade");
            $table->json("kontenVideo_id");
            $table->json("kontenDokumen_id");
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertemuan');
    }
}
