<x-app-layout>
  <div class="container-fluid py-4">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h5 class="mb-4">Assignment Details</h5>
            <div class="row">
              <div class="col-lg-12 mx-auto">
                <h3 class="mt-lg-0 mt-4">{{ $assignment->judul }}</h3>
                <br>
                <h5>Pertemuan Ke- {{ $assignment->pertemuan_id }}</h5>
                <span class="badge badge-success">{{ $assignment->deadline }}</span>
                <br>
                <label class="mt-4">Deskripsi</label>
                <ul>
                  {{ $assignment->deskripsi }}
                </ul>
                <div class="row mt-4">
                  <div class="col-lg-5 mt-lg-0 mt-2">
                      <div class="mb-3">
                        <label for="exampleFormControlSelect1">File Assignmet</label>
                        <input type="file" class="form-control" name="file" required value="">
                      </div>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-lg-5">
                    <button class="btn bg-gradient-primary mb-0 mt-lg-auto w-100" type="button" name="button">Edit Assignment</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="row mt-4">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          <h6>Data Mahasiswa</h6>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nama Mahasiswa</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Assignment</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Grade</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($userAssignment->where('assignment_id', $assignment->id) as $item)
                  <tr>
                    <td>
                      <div class="d-flex px-3 py-1">
                        <div>
                          <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/soft-ui-design-system/assets/img/ecommerce/blue-shoe.jpg" class="avatar me-3" alt="image">
                        </div>
                        <div class="d-flex flex-column justify-content-center">
                          <h6 class="mb-0 text-sm">{{ $item->user->name }}</h6>
                          <p class="text-sm font-weight-bold text-secondary mb-0"><span class="text-success">Submitted</span> assignment</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <a href="{{ $item->assignment }}"></a>
                      <p class="text-sm font-weight-bold mb-0">File Assignment</p>
                    </td>
                    <td class="align-middle text-center text-sm">
                      <p class="text-sm font-weight-bold mb-0">{{ $item->grade }}</p>
                    </td>
                    <td class="align-middle text-end">
                      <div class="d-flex px-3 py-1 justify-content-center align-items-center">
                        <button class="btn btn-link text-dark px-3 mb-0" data-bs-toggle="modal" data-bs-target="#exampleModalSignUp"><i class="fas fa-user-edit text-secondary"></i></button>
                        {{-- <a class="btn btn-link text-dark px-3 mb-0" href="#"><i class="fas fa-user-edit text-secondary"></i></a> --}}
                        <button type="button" class="btn btn-sm btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-3" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Refund rate is lower with 97% than other products">
                          <i class="fas fa-info" aria-hidden="true"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">

    <!-- Grading Modal -->
    <div class="modal fade" id="exampleModalSignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalSignTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body p-0">
            <div class="card card-plain">
              <div class="card-header pb-0 text-left">
                  <h3 class="font-weight-bolder text-primary text-gradient">Grading!</h3>
                  <p class="mb-0">Enter grade and feedback</p>
              </div>
              <div class="card-body pb-3">
                <form role="form text-left">
                  <label>Grade</label>
                  <div class="input-group mb-3">
                    <input type="text" name="grade" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="name-addon">
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Feedback</label>
                    <textarea class="form-control" name="feedback" aria-label="With textarea" name="deskripsi" rows="4" required></textarea>
                  </div>
                  <div class="text-center">
                    <button type="button" class="btn bg-gradient-primary btn-lg btn-rounded w-100 mt-4 mb-0">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  
    {{-- @push('scripts')
    <script>
      tinymce.init({
        selector: ".nonEditableMCE",
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
        readonly: 1
      });
  
      tinymce.init({
        selector: ".editableMCE",
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
  
      });
    </script>
    @endpush --}}
    @push('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('select').selectpicker();
      });
    </script>
    @endpush
  
    {{-- @push('scripts')
    <script>
      const dataTableSearch = new simpleDatatables.DataTable("#datatable-search", {
        searchable: true,
        fixedHeight: true
      });
  
      
      $('.show_confirm').click(function(event) {
              var form =  $(this).closest("form");
              var name = $(this).data("name");
              event.preventDefault();
              swal({
                  title: `Hapus Data?`,
                  text: "Jika data terhapus, data akan hilang selamanya!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  form.submit();
                }
              });
          });
      
    </script>
    @endpush --}}
  
  
  
  </x-app-layout>